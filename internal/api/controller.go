package api

import (
	"context"
	"encoding/json"
	"net/http"

	"github.com/go-chi/chi"

	"bitbucket.org/perfectgentlemande/go-mongodb-crud-api-example/internal/logger"
	"bitbucket.org/perfectgentlemande/go-mongodb-crud-api-example/internal/service"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

type Controller struct {
	srvc *service.Service
}

func NewController(srvc *service.Service) *Controller {
	return &Controller{
		srvc: srvc,
	}
}

func NewServer(srvc *service.Service, log *zap.Logger, prefix, addr string) *http.Server {
	ctrl := NewController(srvc)

	router := chi.NewRouter()
	apiRouter := chi.NewRouter()
	apiRouter.Use(logger.NewLoggingMiddleware(log))
	apiRouter.Get("/user", ctrl.GetUser)
	apiRouter.Post("/user", ctrl.PostUser)
	apiRouter.Put("/user/{id}", ctrl.PutUserByID)
	apiRouter.Delete("/user/{id}", ctrl.DeleteUserByID)

	if prefix == "" {
		prefix = "/"
	}
	router.Route(prefix, func(r chi.Router) {
		r.Mount("/", apiRouter)
	})

	return &http.Server{
		Addr:    addr,
		Handler: router,
	}
}

type APIError struct {
	Message string `json:"message"`
}
type CreatedItem struct {
	ID string `json:"id"`
}

func WriteError(ctx context.Context, w http.ResponseWriter, status int, message string) {
	log := logger.GetLogger(ctx)

	err := RespondWithJSON(w, status, APIError{Message: message})
	if err != nil {
		log.Error("write response", zap.Field{
			Key:       "error",
			Type:      zapcore.ErrorType,
			Interface: err,
		})
	}
}
func WriteSuccessful(ctx context.Context, w http.ResponseWriter, payload interface{}) {
	log := logger.GetLogger(ctx)

	err := RespondWithJSON(w, http.StatusOK, payload)
	if err != nil {
		log.Error("write response", zap.Field{
			Key:       "error",
			Type:      zapcore.ErrorType,
			Interface: err,
		})
	}
}
func RespondWithJSON(w http.ResponseWriter, status int, payload interface{}) error {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	return json.NewEncoder(w).Encode(payload)
}
