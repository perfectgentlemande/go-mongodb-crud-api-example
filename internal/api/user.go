package api

import (
	"encoding/json"
	"net/http"
	"time"

	"github.com/go-chi/chi"

	"bitbucket.org/perfectgentlemande/go-mongodb-crud-api-example/internal/logger"
	"bitbucket.org/perfectgentlemande/go-mongodb-crud-api-example/internal/service"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

type User struct {
	ID        string    `json:"id"`
	Username  string    `json:"username"`
	Email     *string   `json:"email"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

func (u *User) ToService() *service.User {
	return &service.User{
		ID:        u.ID,
		Username:  u.Username,
		Email:     u.Email,
		CreatedAt: u.CreatedAt,
		UpdatedAt: u.UpdatedAt,
	}
}
func usersFromService(usrs []service.User) []User {
	res := make([]User, 0, len(usrs))

	for i := range usrs {
		res = append(res, User{
			ID:        usrs[i].ID,
			Username:  usrs[i].Username,
			Email:     usrs[i].Email,
			CreatedAt: usrs[i].CreatedAt,
			UpdatedAt: usrs[i].UpdatedAt,
		})
	}

	return res
}

func (c *Controller) GetUser(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	log := logger.GetLogger(ctx)

	log.Info("hello get")

	srvcUsrs, err := c.srvc.ListUsers(ctx)
	if err != nil {
		log.Error("cannot list users", zap.Field{
			Key:       "error",
			Type:      zapcore.ErrorType,
			Interface: err,
		})
		WriteError(ctx, w, http.StatusInternalServerError, http.StatusText(http.StatusInternalServerError))
		return
	}

	WriteSuccessful(ctx, w, usersFromService(srvcUsrs))
}
func (c *Controller) PostUser(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	log := logger.GetLogger(ctx)

	log.Info("hello post")

	var usr User
	if err := json.NewDecoder(r.Body).Decode(&usr); err != nil {
		log.Info("wrong employee data", zap.Field{
			Key:       "error",
			Type:      zapcore.ErrorType,
			Interface: err,
		})
		WriteError(ctx, w, http.StatusBadRequest, http.StatusText(http.StatusBadRequest))
		return
	}

	newID, err := c.srvc.CreateUser(ctx, usr.ToService())
	if err != nil {
		log.Error("cannot create user", zap.Field{
			Key:       "error",
			Type:      zapcore.ErrorType,
			Interface: err,
		})
		WriteError(ctx, w, http.StatusInternalServerError, http.StatusText(http.StatusInternalServerError))
		return
	}

	WriteSuccessful(ctx, w, CreatedItem{ID: newID})
}
func (c *Controller) PutUserByID(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	log := logger.GetLogger(ctx)

	log.Info("hello put")
	userID := chi.URLParam(r, "id")
	log = log.With(zap.Field{
		Key:    "user_id",
		Type:   zapcore.StringType,
		String: userID,
	})

	var usr User
	if err := json.NewDecoder(r.Body).Decode(&usr); err != nil {
		log.Info("wrong employee data", zap.Field{
			Key:       "error",
			Type:      zapcore.ErrorType,
			Interface: err,
		})
		WriteError(ctx, w, http.StatusBadRequest, http.StatusText(http.StatusBadRequest))
		return
	}

	err := c.srvc.UpdateUserByID(ctx, userID, usr.ToService())
	if err != nil {
		log.Error("cannot update user", zap.Field{
			Key:       "error",
			Type:      zapcore.ErrorType,
			Interface: err,
		})
		WriteError(ctx, w, http.StatusInternalServerError, http.StatusText(http.StatusInternalServerError))
		return
	}

	WriteSuccessful(ctx, w, struct{}{})
}
func (c *Controller) DeleteUserByID(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	log := logger.GetLogger(ctx)

	log.Info("hello delete")
	userID := chi.URLParam(r, "id")
	log = log.With(zap.Field{
		Key:    "user_id",
		Type:   zapcore.StringType,
		String: userID,
	})

	err := c.srvc.DeleteUserByID(ctx, userID)
	if err != nil {
		log.Error("cannot delete user", zap.Field{
			Key:       "error",
			Type:      zapcore.ErrorType,
			Interface: err,
		})
		WriteError(ctx, w, http.StatusInternalServerError, http.StatusText(http.StatusInternalServerError))
		return
	}

	WriteSuccessful(ctx, w, struct{}{})
}
