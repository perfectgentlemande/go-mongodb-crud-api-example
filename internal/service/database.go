package service

import "context"

type UserStorage interface {
	CreateUser(ctx context.Context, user *User) (string, error)
	ListUsers(ctx context.Context) ([]User, error)
	UpdateUserByID(ctx context.Context, id string, user *User) error
	DeleteUserByID(ctx context.Context, id string) error
}
