package service

import (
	"context"
	"time"

	"github.com/google/uuid"
)

type User struct {
	ID        string
	Username  string
	Email     *string
	CreatedAt time.Time
	UpdatedAt time.Time
}

func (s *Service) ListUsers(ctx context.Context) ([]User, error) {
	return s.userStorage.ListUsers(ctx)
}

func (s *Service) CreateUser(ctx context.Context, user *User) (string, error) {
	now := time.Now()

	user.ID = uuid.NewString()
	user.CreatedAt, user.UpdatedAt = now, now

	return s.userStorage.CreateUser(ctx, user)
}
func (s *Service) UpdateUserByID(ctx context.Context, userID string, user *User) error {
	user.UpdatedAt = time.Now()

	return s.userStorage.UpdateUserByID(ctx, userID, user)
}
func (s *Service) DeleteUserByID(ctx context.Context, userID string) error {
	return s.userStorage.DeleteUserByID(ctx, userID)
}
