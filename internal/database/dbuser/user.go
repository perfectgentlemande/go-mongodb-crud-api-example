package dbuser

import (
	"context"
	"fmt"
	"time"

	"bitbucket.org/perfectgentlemande/go-mongodb-crud-api-example/internal/service"
	"go.mongodb.org/mongo-driver/bson"
)

type User struct {
	ID        string    `bson:"_id"`
	Username  string    `bson:"username"`
	Email     *string   `bson:"email,omitempty"`
	CreatedAt time.Time `bson:"created_at"`
	UpdatedAt time.Time `bson:"updated_at"`
}

func (d *Database) CreateUser(ctx context.Context, user *service.User) (string, error) {
	_, err := d.userCollection.InsertOne(ctx, User{
		ID:        user.ID,
		Username:  user.Username,
		Email:     user.Email,
		CreatedAt: user.CreatedAt,
		UpdatedAt: user.UpdatedAt,
	})

	if err != nil {
		return "", fmt.Errorf("cannot insert user: %w", err)
	}

	return user.ID, nil
}
func (d *Database) ListUsers(ctx context.Context) ([]service.User, error) {
	cur, err := d.userCollection.Find(ctx, bson.M{})
	if err != nil {
		return nil, fmt.Errorf("cannot find users: %w", err)
	}
	defer cur.Close(ctx)

	res := make([]service.User, 0)
	for cur.Next(ctx) {
		usr := User{}

		err = cur.Decode(&usr)
		if err != nil {
			return nil, fmt.Errorf("cannot decode user: %w", err)
		}

		res = append(res, service.User{
			ID:       usr.ID,
			Username: usr.Username,
			Email:    usr.Email,
		})
	}

	return res, nil
}
func (d *Database) UpdateUserByID(ctx context.Context, id string, user *service.User) error {
	// be careful: this way does not unset the old fields (this is actually closer to PATCH request implementation)
	// if you want full-document upload implementation, check docs for MongoDB
	// and probably you should get the existing document on the service layer first
	// to guarantee the full-document upload (keep data like created_at) without knowing how any DB does the update

	_, err := d.userCollection.UpdateOne(ctx,
		bson.M{"_id": id},
		bson.M{
			"&set": User{
				Username:  user.Username,
				Email:     user.Email,
				UpdatedAt: user.UpdatedAt,
			},
		})

	if err != nil {
		return fmt.Errorf("cannot update user: %w", err)
	}

	return nil
}
func (d *Database) DeleteUserByID(ctx context.Context, id string) error {
	_, err := d.userCollection.DeleteOne(ctx, bson.M{"_id": id})

	if err != nil {
		return fmt.Errorf("cannot delete user: %w", err)
	}

	return nil
}
