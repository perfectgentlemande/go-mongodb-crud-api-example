package main

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"os/signal"
	"syscall"

	"bitbucket.org/perfectgentlemande/go-mongodb-crud-api-example/internal/api"
	"bitbucket.org/perfectgentlemande/go-mongodb-crud-api-example/internal/database/dbuser"
	"bitbucket.org/perfectgentlemande/go-mongodb-crud-api-example/internal/logger"
	"bitbucket.org/perfectgentlemande/go-mongodb-crud-api-example/internal/service"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"

	"golang.org/x/sync/errgroup"
)

func main() {
	// TODO FIXME I'm lazy enough to do the yaml config opening today
	const (
		addr   = ":3000"
		prefix = "/backend"
	)

	ctx := context.Background()
	ctx, cancel := signal.NotifyContext(ctx,
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT)
	defer cancel()

	log := logger.DefaultLogger()

	dbUser, err := dbuser.NewDatabase(ctx, &dbuser.Config{
		DBName:  "test",
		ConnStr: "mongodb://localhost:55004",
	})
	if err != nil {
		log.Fatal("cannot create db", zap.Field{
			Key:       "error",
			Type:      zapcore.ErrorType,
			Interface: err,
		})
	}

	defer dbUser.Close(ctx)

	err = dbUser.Ping(ctx)
	if err != nil {
		log.Fatal("cannot ping db", zap.Field{
			Key:       "error",
			Type:      zapcore.ErrorType,
			Interface: err,
		})
	}

	srvc := service.NewService(dbUser)
	srv := api.NewServer(srvc, log, prefix, addr)

	rungroup, ctx := errgroup.WithContext(ctx)
	log.Info("starting server", zap.Field{
		Key:    "port",
		Type:   zapcore.StringType,
		String: addr,
	})
	rungroup.Go(func() error {
		if errSrv := srv.ListenAndServe(); errSrv != nil && !errors.Is(errSrv, http.ErrServerClosed) {
			return fmt.Errorf("listen and serve error: %w", errSrv)
		}

		return nil
	})
	rungroup.Go(func() error {
		<-ctx.Done()

		if errShutdown := srv.Shutdown(context.Background()); errShutdown != nil {
			return fmt.Errorf("shutdown http server %w", errShutdown)
		}

		return nil
	})
	err = rungroup.Wait()
	if err != nil {
		log.Fatal("run group exited because of error", zap.Field{
			Key:       "error",
			Type:      zapcore.ErrorType,
			Interface: err,
		})
	}

	log.Info("server exited properly")
}
